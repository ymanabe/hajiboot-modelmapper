# README #

はじめてのSpring Boot の第3章 「Spring Boot」による「Web アプリ開発」で利用しているBeanUtilsをModelMapperへ変更したソースコードです。ベースにしたソースコードは下記の通り はじめてのSpring Bootのリポジトリ 3.3.5.2 となっています。

[はじめてのSpring Bootのリポジトリ 3.3.5.2 ](https://github.com/making/hajiboot-samples/tree/master/chapter03/3.3.5.2_hajiboot-thymeleaf)